# Define the provider for AWS
provider "aws" {
  region = "us-west-2" # Replace with your desired region
}

# Create an IAM user
resource "aws_iam_user" "superuser" {
  name = "dimi" # Replace with your desired username
}

# Create an IAM access key and secret for the user
resource "aws_iam_access_key" "superuser_access_key" {
  user = aws_iam_user.superuser.name
}

# Grant the user AdministratorAccess policy
resource "aws_iam_user_policy_attachment" "superuser_policy_attachment" {
  user       = aws_iam_user.superuser.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

# Grant the user root access key and secret
resource "aws_iam_user_login_profile" "superuser_login_profile" {
  user                    = aws_iam_user.superuser.name
  password_reset_required = true
}

# Output the access key and secret for the user
output "access_key" {
  value     = aws_iam_access_key.superuser_access_key.id
  sensitive = false
}

output "secret_key" {
  value     = aws_iam_access_key.superuser_access_key.secret
  sensitive = true
}
