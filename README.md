# deploy aws services within Terraform.

```
cd existing_repo
git remote add origin https://gitlab.com/devopsera11/aws.git
git branch -M main
git push -uf origin main
```
***

## Name
aws deployments, within Terraform

## Description
Deploy aws services, within Terraform.

## Authors and acknowledgment
Dimitrios Sakellaropoulos, DevOps Engineer.
