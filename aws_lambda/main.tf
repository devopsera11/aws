# Define the provider for AWS
provider "aws" {
  region = "us-west-2" # Replace with your desired region
}

# Create an AWS Lambda function
resource "aws_lambda_function" "my_lambda_function" {
  function_name    = "my-lambda-function" # Replace with your desired function name
  runtime          = "python3.8"
  handler          = "server.lambda_handler"
  timeout          = 10  # Replace with your desired timeout in seconds
  memory_size      = 128 # Replace with your desired memory size in megabytes
  role             = aws_iam_role.lambda_execution_role.arn
  source_code_hash = filebase64sha256("./server.py") # Replace with the path to your server.py code zip file

  environment {
    variables = {
      aws_access_key_id     = " xxxxxxxxxxx"
      aws_secret_access_key = "xxxxxxxxxxx"
    }
  }
}

# IAM role for the Lambda function execution
resource "aws_iam_role" "lambda_execution_role" {
  name = "my-lambda-execution-role" # Replace with your desired role name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# IAM policy for the Lambda function execution role
resource "aws_iam_policy" "lambda_execution_policy" {
  name        = "my-lambda-execution-policy" # Replace with your desired policy name
  description = "Policy for Lambda function execution role"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*"
    }
  ]
}
EOF
}

# Attach the Lambda execution policy to the execution role
resource "aws_iam_role_policy_attachment" "lambda_execution_policy_attachment" {
  role       = aws_iam_role.lambda_execution_role.name
  policy_arn = aws_iam_policy.lambda_execution_policy.arn
}
