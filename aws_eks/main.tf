# Define the provider for AWS
provider "aws" {
  region = "us-west-2" # Replace with your desired region
}

# Create an EKS cluster
resource "aws_eks_cluster" "eks_cluster" {
  name     = "my-eks-cluster" # Replace with your desired cluster name
  role_arn = aws_iam_role.eks_cluster_role.arn
  version  = "1.21" # Replace with your desired EKS version

  vpc_config {
    subnet_ids         = ["subnet-xxxxxxxx", "subnet-yyyyyyyy"] # Replace with your desired subnet IDs
    security_group_ids = ["sg-xxxxxxxx"]                        # Replace with your desired security group IDs
  }

  depends_on = [aws_iam_role_policy_attachment.eks_cluster]
}

# Create an IAM role for EKS cluster
resource "aws_iam_role" "eks_cluster_role" {
  name = "my-eks-cluster-role" # Replace with your desired role name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# Attach the necessary policies to the IAM role for EKS cluster
resource "aws_iam_role_policy_attachment" "eks_cluster" {
  role       = aws_iam_role.eks_cluster_role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

# Create a Kubernetes Deployment and Service
resource "kubernetes_deployment" "webserver_deployment" {
  metadata {
    name = "webserver-deployment"
  }

  spec {
    replicas = 3 # Replace with your desired number of replicas

    selector {
      match_labels = {
        app = "webserver"
      }
    }

    template {
      metadata {
        labels = {
          app = "webserver"
        }
      }

      spec {
        container {
          name  = "webserver"
          image = "jimakosak/webserver" # Replace with your desired Docker image
          port {
            container_port = 80
          }
        }
      }
    }
  }
}

# Create a Kubernetes Service to expose the Deployment
resource "kubernetes_service" "webserver_service" {
  metadata {
    name = "webserver-service"
  }

  spec {
    selector = {
      app = kubernetes_deployment.webserver_deployment.metadata.0.labels.app
    }

    port {
      port        = 80
      target_port = 80
    }

    type = "LoadBalancer"
  }
}
